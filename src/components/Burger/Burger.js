import React from 'react';
import classes from './Burger.css';
import BurgerIngrediant from './BurgerIngredient/BurgerIngredient';

const burger = (props) => {
 
    let ingrediantBurger =  Object.keys(props.ingredients)
    .map(key => {
            return [...Array(props.ingredients[key])].map((element,i)=>{
                return <BurgerIngrediant key={key + i} type={key}/>
            })
    })
    .reduce((arr,ele)=>{
        return arr.concat(ele)
    })
    if(ingrediantBurger.length === 0){
        ingrediantBurger = <p>please select ingredients</p>
    }

   // console.log(ingrediantBurger);

   
    
    return (
        <div className={classes.Burger}>
            <BurgerIngrediant type='burger-top' />
            {ingrediantBurger}
            <BurgerIngrediant type='burger-bottom' />
        </div>

    )

}

export default burger;