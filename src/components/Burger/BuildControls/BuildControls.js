import React from 'react';

import classes from './BuildControls.css';

import BuildControl from './BuildControl/BuildControl';

const controls = [
    { label: 'Salad', type: 'salad' },
    { label: 'Cheese', type: 'cheese' },
    { label: 'Bacon', type: 'bacon' },
    { label: 'Meat', type: 'meat' }
];

const buildControls = (props) => {
  return(  <div className={classes.BuildControls}>
        <p> Value : <strong>{props.price.toFixed(2)}</strong></p>
        {controls.map(ctrl => {
           return <BuildControl 
           key={ctrl.label} 
           label={ctrl.label}
            added={() => props.ingredientAdded(ctrl.type)}
            remove={() => props.ingredientRemove(ctrl.type)}
            disable={props.ingredientDisable[ctrl.type]}/>
        })}
        <button className={classes.OrderButton} disabled={props.disableOrder}
        onClick={props.ordered}>ORDER NOW</button>
    </div>)
}

export default buildControls;