import React from 'react';

import classes from './Model.css';
import Backdrop from '../Backdrop/Backdrop';
import Aux from '../../../hoc/Aux';

const Model = (props) => {
    return (
   <Aux>
         <Backdrop show={props.show} clicked={props.closeModel}/>
    <div className={classes.Model}
        style={{
            transform: props.show ? 'translateY(0)' : 'translateY(-100vh)'
        }}>
       
        {props.children}
    </div>
    </Aux>)
}

export default Model;