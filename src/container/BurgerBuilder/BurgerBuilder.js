
import React, { Component } from 'react';
import Aux from '../../hoc/Aux';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls'
import OrderSummary from '../../components/Burger/BurgerIngredient/OrderSummary/OrderSummary';
import Model from '../../components/UI/Model/Model';
import Spinner from '../../components/UI/Spinner/Spinner';
import axios from '../../axios-orders';
const INGREDIENT_PRICE = {
    cheese: 0.4,
    meat: 1.2,
    salad: 0.7,
    bacon: 2.0
}
class BurgerBuilder extends Component {

    state = {
        ingredients:null,
        price: 0,
        purchasing: false,
        purchasable: false,
        error: false,
        loading: false
    }
    componentDidMount() {
        axios.get('/ingredients.json')
            .then(response => {
                this.setState({ ingredients: response.data })
            })
            .catch(error => {

            })
    }
    updatePurchaseState(ingredients) {
        const sum = Object.keys(ingredients)
            .map(igKey => {
                return ingredients[igKey];
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0);
        this.setState({ purchasable: sum > 0 })

    }

    addIngredientHandler = (type) => {
        const oldCount = this.state.ingredients[type];
        const updatedCount = oldCount + 1;
        const updatedIngredients =
            { ...this.state.ingredients };
        updatedIngredients[type] = updatedCount;
        const priceAddition = INGREDIENT_PRICE[type];
        const oldPrice = this.state.price;
        const newPrice = oldPrice + priceAddition;
        this.setState({ price: newPrice, ingredients: updatedIngredients })
        this.updatePurchaseState(updatedIngredients);
    }

    removeIngredientHandler = (type) => {
        const oldCount = this.state.ingredients[type];
        if (oldCount <= 0) {
            return;
        }
        const updatedCount = oldCount - 1;
        const updatedIngredients = { ...this.state.ingredients };
        updatedIngredients[type] = updatedCount;
        const priceDeducation = INGREDIENT_PRICE[type];
        const oldPrice = this.state.price;
        const newPrice = oldPrice - priceDeducation;
        this.setState({ price: newPrice, ingredients: updatedIngredients })
        this.updatePurchaseState(updatedIngredients);
    }

    purchaseHandler = () => {
        this.setState({ purchasing: true })
    }
    removepurchaseHandler = () => {
        this.setState({ purchasing: false })
    }
    continuepurchaseHandler = () => {
        // alert('you can continue');
        this.setState({ loading: true });
        const order = {
            ingredients: this.state.ingredients,
            price: this.state.totalPrice,
            customer: {
                name: 'jaydeep kataria',
                address: {
                    street: 'Ahmedabad',
                    zipCode: '380015',
                    country: 'India'
                },
                email: 'jaydeepkataria@gmail.com'
            },
            deliveryMethod: 'fastest'
        }
        axios.post('/orders.json', order)
            .then(response => {
                this.setState({ loading: false, purchasing: false });
            })
            .catch(error => {
                this.setState({ loading: false, purchasing: false });
            });
    }
    render() {
        const disableIngredients = {
            ...this.state.ingredients
        }
        for (let key in disableIngredients) {
            disableIngredients[key] = disableIngredients[key] <= 0
        }
        let orderSummary = null;
        // console.log(this.state.price);
        const purchaseOrder = this.state.price > 0;
       
        let burger = this.state.error ? <p>Ingredients can't be loaded!</p> : <Spinner />;
        if (this.state.ingredients) {
            burger = (
                <Aux>
                    <Burger ingredients={this.state.ingredients} price={this.state.price} />
                    <BuildControls
                        ingredientAdded={this.addIngredientHandler}
                        ingredientRemove={this.removeIngredientHandler}
                        ingredientDisable={disableIngredients}
                        price={this.state.price}
                        disableOrder={!purchaseOrder}
                        ordered={this.purchaseHandler}></BuildControls>
                </Aux>
            )
            orderSummary = <OrderSummary ingredients={this.state.ingredients}
            purchaseCancled={this.removepurchaseHandler}
            purchaseContinue={this.continuepurchaseHandler} />;
        }
        if (this.state.loading) {
            orderSummary = <Spinner />;
        }
        return (
            <Aux>
                <Model show={this.state.purchasing} closeModel={this.removepurchaseHandler}>
                    {orderSummary}
                </Model>
                {burger}
            </Aux>
        )
    }
}

export default BurgerBuilder;